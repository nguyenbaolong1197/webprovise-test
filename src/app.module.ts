import { Module } from '@nestjs/common';

import { AppService } from './app.service';
import { CompanyService } from './company/company.service';

import { CompanyModule } from './company/company.module';

import { CompanyController } from './company/company.controller';
import { AppController } from './app.controller';

@Module({
  imports: [CompanyModule],
  controllers: [AppController, CompanyController],
  providers: [AppService, CompanyService],
})
export class AppModule {}
