export const THIRD_PARTY_API = {
  GET_ALL_COMPANIES:
    'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies',
  GET_ALL_TRAVELLERS:
    'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels',
};
