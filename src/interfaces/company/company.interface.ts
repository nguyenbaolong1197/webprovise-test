export interface CompanyInterface {
  id: string;
  createdAt: Date;
  name: string;
  parentId?: string;
  children?: Array<CompanyInterface>;
  cost?: string;
}
