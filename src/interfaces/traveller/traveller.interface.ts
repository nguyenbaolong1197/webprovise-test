export interface TravellerInterface {
  id: string;
  createdAt: Date;
  employeeName: string;
  departure: string;
  destination: string;
  price: string;
  companyId: string;
}
