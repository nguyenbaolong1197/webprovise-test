import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import axios from 'axios';

import { THIRD_PARTY_API } from 'src/constants/thirdPartyAPI';

import { CompanyInterface } from 'src/interfaces/company/company.interface';
import { TravellerInterface } from 'src/interfaces/traveller/traveller.interface';

@Injectable()
export class CompanyService {
  private companies: CompanyInterface[];
  private travellers: TravellerInterface[];
  public THIRD_PARTY_API = THIRD_PARTY_API;

  private async fetchCompanyData(): Promise<any[]> {
    return this.fetchData(THIRD_PARTY_API.GET_ALL_COMPANIES);
  }

  private async fetchTravelData(): Promise<any[]> {
    return this.fetchData(THIRD_PARTY_API.GET_ALL_TRAVELLERS);
  }

  private async fetchData(url: string): Promise<any[]> {
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      return error;
    }
  }

  async processCompanyData(): Promise<any[]> {
    try {
      //Fetch the data;
      this.companies = await this.fetchCompanyData();
      if (!this.companies || !this.companies.length) {
        throw new HttpException('Companies not found', HttpStatus.NOT_FOUND);
      }
      this.travellers = await this.fetchTravelData();
      if (!this.travellers || !this.travellers.length) {
        throw new HttpException('Travellers not found', HttpStatus.NOT_FOUND);
      }

      const companiesWithChildrenAndCost = this.processChildrenOfCompany();

      return companiesWithChildrenAndCost;
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'Failed to process company data',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
        {
          cause: error,
        },
      );
    }
  }

  // Recursively process the companies and create a nested array with children with cost
  private processChildrenOfCompany(
    parentId: string = null,
  ): Array<CompanyInterface> {
    const nestedCompanies = [];

    this.companies.forEach((company) => {
      if (company.parentId === parentId || !parentId) {
        // Recursion stop when company has no child;
        const children = this.processChildrenOfCompany(company.id);
        const cost = this.calculateTravelCost(company.id);
        const companyWithChildren = { ...company, children, cost };
        nestedCompanies.push(companyWithChildren);
      }
    });

    return nestedCompanies;
  }

  // Recursively calculate travel cost of children and parents
  private calculateTravelCost(companyId: string): number {
    const company = this.companies.find((c) => c.id === companyId);
    if (!company) {
      return 0;
    }
    const travelCost = this.calculateCompanyTravelCostById(companyId);

    const children = this.companies.filter((c) => c.parentId === companyId);
    // Recursion stop when no child found(children is an empty array);
    const childrenTravelCost = children.reduce(
      (total, child) => total + this.calculateTravelCost(child.id),
      0,
    );

    return travelCost + childrenTravelCost;
  }

  private calculateCompanyTravelCostById(companyId: string): number {
    const cost = this.travellers
      .filter((traveller) => {
        return traveller.companyId === companyId;
      })
      .reduce(
        (total, currentTraveller) => total + parseFloat(currentTraveller.price),
        0,
      );
    return cost;
  }
}
