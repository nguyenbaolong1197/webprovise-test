import { Controller, Get } from '@nestjs/common';
import { CompanyService } from './company.service';

@Controller('companies')
export class CompanyController {
  constructor(private companyService: CompanyService) {}

  @Get('')
  async getProcessedCompanyData() {
    return this.companyService.processCompanyData();
  }
}
